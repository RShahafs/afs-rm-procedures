USE [New_CentralDB_R]
GO
/****** Object:  StoredProcedure [dbo].[Willie_Test]    Script Date: 5/28/2020 1:25:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[Willie_Test]
AS
DECLARE
@PersonID	UNIQUEIDENTIFIER

BEGIN
	SELECT 
		p.ENGLISH_FIRSTNAME,P.ENGLISH_LASTNAME
		,P.PERSON_REF
		, P.id
	FROM PERSON p
	WHERE p.IOC_CODE = 'USA'
		AND P.ENGLISH_FIRSTNAME LIKE 'Wi%'
		and P.ENGLISH_LASTNAME LIKE 'T%'

END